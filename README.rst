Example run:

Find a schedule 1 times for an instance having 30 devices and 24 time
slots and there are dependencies between devices. The instance
should be generated in the corresponding folder (log/30a24t)

```
python dep.py 1 30 24
```
